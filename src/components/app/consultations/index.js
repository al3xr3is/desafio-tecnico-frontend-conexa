import DialogConsultForm from './DialogConsultForm'
import DialogConsult from './DialogConsult'
import DataConsultations from './DataConsultations'

export {
  DialogConsult,
  DialogConsultForm,
  DataConsultations,
}
