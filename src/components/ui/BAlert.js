import Vue from 'vue'
import { VSnackbar, VBtn, VIcon } from 'vuetify/lib'

const DEFAULT_TIMEOUT = 5000
const DEFAULT_COLOR = 'black'

const snackData = Vue.observable({
  message: '',
  timeout: DEFAULT_TIMEOUT,
  open: false,
  color: DEFAULT_COLOR,
})

export const closeAlert = () => {
  snackData.message = ''
  snackData.timeout = DEFAULT_TIMEOUT
  snackData.open = false
  snackData.color = DEFAULT_COLOR
}

export const showAlert = (message, color = DEFAULT_COLOR, timeout = DEFAULT_TIMEOUT) => {
  snackData.message = message
  snackData.timeout = timeout
  snackData.open = true
  snackData.color = color
}

const RenderCloseButton = () => {
  return (
    <VBtn
      icon
      vOn:click={closeAlert}
    >
      <VIcon>
        mdi-close-circle-outline
      </VIcon>
    </VBtn>
  )
}

export default {
  name: 'BAlert',
  functional: true,
  render() {
    return (
      <VSnackbar
        value={snackData.open}
        timeout={snackData.timeout}
        right
        bottom
        vOn:input={(value) => { snackData.open = value }}
        scopedSlots={{
          action: () => <RenderCloseButton />,
        }}
      >
        {snackData.message}
      </VSnackbar>
    )
  },
}
