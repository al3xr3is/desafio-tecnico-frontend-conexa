import Vue from 'vue'
import { VDialog } from 'vuetify/lib'

const dialogData = Vue.observable({
  component: null,
  props: {},
  propsDialog: {},
  open: false,
})

export const showDialog = (component, props, propsDialog = {}) => {
  dialogData.component = component
  dialogData.props = { ...props }
  dialogData.propsDialog = propsDialog
  dialogData.open = true
}

export const closeDialog = () => {
  dialogData.component = null
  dialogData.props = {}
  dialogData.propsDialog = {}
  dialogData.open = false
}

const clickOutsideEvent = () => {
  if (dialogData.propsDialog.persistent) {
    return
  }
  closeDialog()
}

export default {
  name: 'BDialog',
  render() {
    const Component = dialogData.component
    return (
      <VDialog
        max-width={500}
        value={dialogData.open}
        vOn:input={(value) => { dialogData.open = value }}
        vOn:click-oustide={clickOutsideEvent}
        {...{
          attrs: dialogData.propsDialog,
        }}
      >
        <Component {...{ attrs: dialogData.props }} />
      </VDialog>
    )
  },
}
