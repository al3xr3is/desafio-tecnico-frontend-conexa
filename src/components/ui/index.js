import BButton from './BButton'
import BInput from './BInput'
import MenuSideBar from './menu/MenuSideBar'
import MenuTopBar from './menu/MenuTopBar'
import BData from './BData'

import BAlert, {
  showAlert,
  closeAlert,
} from './BAlert'

import BDialog, {
  showDialog,
  closeDialog,
} from './BDialog'

export {
  BAlert,
  BButton,
  BDialog,
  BData,
  BInput,
  MenuSideBar,
  MenuTopBar,
  showDialog,
  closeDialog,
}

export const UiPlugin = (Vue) => {
  Vue.prototype.$ui = {
    showAlert,
    closeAlert,
    showDialog,
    closeDialog,
  }
}
