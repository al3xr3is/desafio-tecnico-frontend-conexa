import Vue from 'vue'
import {
  BButton,
  BInput,
  BData,
  UiPlugin,
} from './components/ui'
import App from './App'
import './registerServiceWorker'
import '@/utils/http'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import './assets/style.css'

Vue.config.productionTip = false

Vue.component('b-button', BButton)
Vue.component('b-input', BInput)
Vue.component('b-data', BData)
Vue.use(UiPlugin)

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
