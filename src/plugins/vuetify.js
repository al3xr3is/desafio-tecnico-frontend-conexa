import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#038C8C',
        secondary: '#03A696',
        stronger: '#035AA6',
        lighter: '#4192D9',
      },
    },
  },
});
