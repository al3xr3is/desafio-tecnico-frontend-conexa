import Vue from 'vue'
import VueRouter from 'vue-router'
import middleware from './middleware'

Vue.use(VueRouter)

export const routes = {
  path: '/home',
  name: 'home.panel',
  component: () => import(/* webpackChunckName: "home" */ '../pages/Panel.vue'),
  children: [
    {
      path: '',
      name: 'home.inicio',
      meta: {
        label: 'Consultas',
        icon: 'mdi-medical-bag',
      },
      component: () => import(/* webpackChunckName: "home.consults" */ '../pages/Consultants.vue'),
    },
  ],
}

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    routes,
    {
      path: '/login',
      name: 'login.page',
      component: () => import(/* webpackChunckName: "login" */ '../pages/Login.vue'),
    },
  ],
})

middleware(router)

export default router
