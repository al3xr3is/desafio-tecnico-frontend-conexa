import {
  userService,
} from '@/services'
import store from '@/store'
import httpClient from '@/utils/http'

const HOME_URL = '/home'
const LOGIN_URL = '/login'
const LOGOUT_URL = '/logout'

const setUserCredentialsIfNot = (credentials) => {
  if (!store.getters.userIsDefined) {
    httpClient.setToken(credentials.token)
    store.dispatch('setUserIfNotSetted', credentials)
  }
}

export default (router) => {
  Object.defineProperties(router, {
    goHome: {
      value() {
        router.push(HOME_URL)
      },
    },
    goLogin: {
      value() {
        router.push(LOGIN_URL)
      },
    },
  })

  router.beforeEach((to, from, next) => {
    const userIsLogged = userService.userIsLogged()
    if (to.path === LOGIN_URL) {
      return next()
    }

    if (to.path === LOGOUT_URL) {
      userService.logout()
      return next(LOGIN_URL)
    }

    if (to.path === '/') {
      return next(HOME_URL)
    }

    if (userIsLogged) {
      setUserCredentialsIfNot(userService.getUserDataFromStorage())
      return next()
    }

    return next(LOGIN_URL)
  })
}
