import httpClient from '@/utils/http'

const BASE_ENDPOINT = '/consultas'
const BASE_API_ENDPOINT = '/consulta'

export default {
  addConsult({
    dataConsulta,
    idMedico,
    observacao,
    paciente,
  }) {
    return httpClient.post(`${BASE_API_ENDPOINT}`, {
      dataConsulta: new Date(dataConsulta).toISOString(),
      idMedico,
      observacao,
      paciente,
    })
  },
  getAllConsultations() {
    return httpClient.get(`${BASE_ENDPOINT}`)
      .then((d) => d.data.data)
  },
  getConsult(consultId) {
    return httpClient.get(`${BASE_API_ENDPOINT}/${consultId}`)
      .then((d) => d.data.data)
  },
}
