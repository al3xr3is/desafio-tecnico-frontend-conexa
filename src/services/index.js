import userService from './user'
import consultationsService from './consultations'

export {
  userService,
  consultationsService,
}
