import httpClient from '@/utils/http'
import * as storage from '@/utils/storage'
import store from '@/store'

const storagePrefix = '_user'

export default {
  loginWithUsernameAndPassword(username, password) {
    return httpClient.post('/login', {
      username,
      password,
    })
      .then((d) => d.data.data)
      .then((d) => {
        this.storeUserData(d)
        httpClient.setToken(d.token)
      })
  },

  logout() {
    storage.clearStorage()
  },

  storeUserData({ email, nome, token }) {
    storage.storeWithPrefix({ email, nome, token }, storagePrefix)
    store.dispatch('setLoggedUser', { email, nome, token })
  },

  getUserDataFromStorage() {
    return storage.getAllByPrefix(storagePrefix, 'email', 'nome', 'token')
  },

  userIsLogged() {
    return storage.hasItem(`${storagePrefix}.token`)
  },
}
