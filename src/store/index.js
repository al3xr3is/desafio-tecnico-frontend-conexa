import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const STORE_USER_MUTATION = 'storeUser'
const FLUSH_USER_MUTATION = 'flushUser'

export default new Vuex.Store({
  state: {
    user: {
      token: '',
      email: '',
      nome: '',
    },
  },
  mutations: {
    [STORE_USER_MUTATION](state, { token, email, nome }) {
      state.user = {
        token,
        email,
        nome,
      }
    },
    [FLUSH_USER_MUTATION](state) {
      state.user = {
        token: '',
        email: '',
        nome: '',
      }
    },
  },
  actions: {
    setLoggedUser({ commit }, user) {
      commit(STORE_USER_MUTATION, user)
    },
    destroyLoggedUser({ commit }) {
      commit(FLUSH_USER_MUTATION)
    },
    setUserIfNotSetted({ commit, state }, user) {
      if (state.user.nome === '') {
        commit(STORE_USER_MUTATION, user)
      }
    },
  },
  getters: {
    getUserDetails(state) {
      return state.user
    },
    userIsDefined(state) {
      return state.user.token !== ''
    },
  },
  modules: {
  },
});
