// eslint-disable-next-line import/prefer-default-export
export const getFormFields = (formObject) => {
  const data = {}
  new FormData(formObject).forEach((value, field) => { data[field] = value })
  return data
}
