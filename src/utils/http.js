import axios from 'axios'

axios.defaults.headers.post['Content-Type'] = 'application/json'

const httpClient = axios.create({
  baseURL: 'http://desafio.conexasaude.com.br/api',
  crossDomain: true,
})

Object.defineProperties(httpClient, {
  setToken: {
    value(token) {
      httpClient.defaults.headers.common.Authorization = `Bearer ${token}`
    },
  },
  clearToken: {
    value() {
      httpClient.defaults.headers.common.Authorization = null
    },
  },
})

export default httpClient
