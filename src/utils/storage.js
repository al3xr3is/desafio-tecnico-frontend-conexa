export const hasItem = (key) => !!window.localStorage.getItem(key)

export const getItem = (key) => window.localStorage.getItem(key)

export const setItem = (key, value) => window.localStorage.setItem(key, value)

export const removeItem = (key) => window.localStorage.removeItem(key)

export const clearStorage = () => window.localStorage.clear()

export const storeWithPrefix = (object, prefix) => {
  Object.keys(object).forEach((item) => setItem(`${prefix}.${item}`, object[item]))
}

export const getAllByPrefix = (prefix, ...keys) => {
  return keys.reduce((items, key) => {
    items[key] = getItem(`${prefix}.${key}`)
    return items
  }, {})
}
